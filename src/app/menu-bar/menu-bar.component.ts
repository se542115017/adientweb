import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { AuthenticationService } from '@app/_services';
import {MenuService} from '@app/_helpers/menu-service/menu.service'

@Component({
  selector: 'app-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.less']
})
export class MenuBarComponent implements OnInit {
  @Output() openHide = new EventEmitter<boolean>();

  constructor(private authenticationService: AuthenticationService,public menu: MenuService) {

  }

  ngOnInit(): void {
  }
  logout() {
    
    this.authenticationService.logout();

  }

  hide(){
    this.openHide.emit(false);
  }

}
