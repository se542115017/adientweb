import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
export class MenuService {

    private selected = new Subject<boolean>();
    public selected$ = this.selected.asObservable();
    is_hide:boolean = false;

  constructor() {
      
  }

  hide(is_hide){
    this.is_hide = !is_hide
    this.selected.next(this.is_hide);
  }
  
}